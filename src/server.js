require('dotenv').config()
const express = require('express');
const app = express();

app.get('/', (req,res) => {
    res.send('Server is ready for request.')
})

const server = app.listen(process.env.PORT, () => {
console.log(`Server is running on http://localhost:${process.env.PORT}`)
});

//app.listen is taking a two arguments first is port and second is callback function that doesn't take any argument. this callback funcition runs after the server.